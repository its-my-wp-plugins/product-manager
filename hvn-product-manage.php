<?php
/**
 * Product manage programmatically
 *
 * Plugin Name: Product manage programmatically
 * Plugin URI:  https://no-site.org/
 * Description: Create and update product programmatically
 * Version:     1.0
 * Author:      Haven
 * Author URI:  https://no-ste.org/
 * Text Domain: prmpr
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Invalid request.' );
}

define( 'PRMPR_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'PRMPR_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

include_once PRMPR_PLUGIN_DIR . 'shortcodes/shortcodes.php';
include_once PRMPR_PLUGIN_DIR . 'product-manage-class.php';