(function pmprAjaxForm() {
  const forms = Array.from(document.querySelectorAll("[data-prmpr-form]"));
  const formsObj = forms.map(function (form) {
    return {
      $: form,
      btn: form.querySelector("[data-prmpr-form-submit]"),
      isSending: false,
      status: (function () {
        let errorNode = null;

        function reset() {
          if (errorNode) {
            errorNode.remove();
          }
        }

        function set(form, message, type) {
          reset();
          errorNode = document.createElement("div");
          errorNode.classList.add("prmpr-form-message");
          errorNode.classList.add("prmpr-form-message_" + type);
          errorNode.innerText = message;
          form.append(errorNode);
        }

        return {
          set,
          reset,
        };
      })(),
    };
  });

  function removeAllErrors(form) {
    const errorsNodes = Array.from(form.querySelectorAll(".prmpr-field-error"));
    errorsNodes.forEach(function (error) {
      error.remove();
    });
  }

  function generateFieldErrorMessage(field, message) {
    const errorNode = document.createElement("div");
    errorNode.classList.add("prmpr-field-error");
    errorNode.innerText = message;
    jQuery(field).closest(".prmpr-field-wrap").append(errorNode);
  }

  function validateForm(form) {
    let isValid = true;
    const reqFields = Array.from(form.querySelectorAll(".prmpr-required-field"));
    const names = Array.from(new Set(reqFields.map((field) => field.name).filter((name) => !!name.length)));

    names.forEach(function (name) {
      const field = form[name];
      switch (field.type || field[0].type) {
        case "file":
          if (!field.files.length) {
            isValid = false;
            generateFieldErrorMessage(field, "Field is required");
          }
          break;
        case "radio":
          if (!field.value.length) {
            isValid = false;
            generateFieldErrorMessage(field[0], "Field is required");
          }
          break;
        default:
          if (!field.value.length) {
            isValid = false;
            generateFieldErrorMessage(field, "Field is required");
          }
          break;
      }
    });

    return isValid;
  }

  function getFormData(form) {
    const data = new FormData();
    const fields = Array.from(form.elements);
    const names = Array.from(new Set(fields.map((field) => field.name).filter((name) => !!name.length)));

    names.forEach(function (name) {
      const field = form[name];
      switch (field.type || field[0].type) {
        case "file":
          if (field.files.length) {
            data.append(field.name, field.files[0]);
          }
          break;
        case "radio":
          data.append(field[0].name, field.value);
          break;
        case "submit":
          break;
        default:
          data.append(field.name, field.value);
          break;
      }
    });

    return data;
  }

  function sendFormData(formObj) {
    const form = formObj.$;
    const btn = formObj.btn;

    if (formObj.isSending) return;

    removeAllErrors(form);
    formObj.status.reset();

    if (!validateForm(form)) {
      formObj.status.set(form, "Fill in the fields correctly", "error");
      return;
    }

    const data = getFormData(form);

    data.append("action", "create_product");
    data.append("nonce", prmprajax.nonce);

    jQuery.ajax({
      url: prmprajax.url,
      type: "POST",
      data: data,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      beforeSend: function () {
        btn.disabled = true;
        formObj.isSending = true;

        formObj.status.set(form, "Sending...", "notice");
      },
      success: function (res) {
        // alert(res);
        btn.disabled = false;
        formObj.isSending = false;
        if (res.success) {
          formObj.status.set(form, res.form_status, "success");
          if (!form["post_id"]) {
            form.reset();
            form["post_thumbnail"].dispatchEvent(new Event("change"));
            form["post_apartment_country"].dispatchEvent(new Event("change"));
            jQuery(form["post_apartment_country"]).trigger("select2:select");
          }

          if (btn.dataset.redirectTo) {
            setTimeout(function () {
              window.location = btn.dataset.redirectTo;
            }, 1000);
          }
        } else {
          formObj.status.set(form, res.form_status, "error");
          res.reqired_fields.forEach(function (fieldMeta) {
            generateFieldErrorMessage(form[fieldMeta.key], fieldMeta.message);
          });
        }
      },
      error: function (res, type, message) {
        btn.disabled = false;
        formObj.isSending = false;
        formObj.status.set(form, message, "error");
      },
    });

    // console.log(data);
  }

  function addListeners() {
    formsObj.forEach(function (formObj) {
      formObj.$.addEventListener("submit", function (event) {
        event.stopPropagation();
        event.preventDefault();
        sendFormData(formObj);
      });
    });
  }

  addListeners();
})();

(function deletePost() {
  const btns = Array.from(document.querySelectorAll(["[data-delete-post]"]));

  function init(btn) {
    btn.addEventListener("click", function (e) {
      e.preventDefault();
      $post_id = btn.dataset.deletePost;
      if (!$post_id) return;

      const data = new FormData();
      data.append("action", "delete_product");
      data.append("nonce", prmprajax.nonce);
      data.append("post_id", $post_id);

      jQuery.ajax({
        url: prmprajax.url,
        type: "POST",
        data: data,
        cache: false,
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function () {
          btn.disabled = true;
        },
        success: function (res) {
          btn.disabled = false;
          if (res.success) {
            window.location.reload();
          }
        },
        error: function () {
          btn.disabled = false;
        },
      });
    });
  }

  btns.forEach(init);
})();

(function stepLocation() {
  const countriesSelect = document.querySelector("[data-select-countries]");
  const citiesSelect = document.querySelector("[data-select-cities]");

  if (!countriesSelect || !citiesSelect) return;

  let allCountries = [];

  function loadData(slug) {
    return new Promise(function (resolve, reject) {
      fetch("/wp-content/uploads/locations/" + slug + ".json")
        .then(function (res) {
          return res.json();
        })
        .then(function (res) {
          resolve(res);
        })
        .catch(function (error) {
          reject(error);
        });
    });
  }

  function generateOptions(data, select, defaultOptionName) {
    select.innerHTML = "";
    let option = null;
    if (defaultOptionName) {
      option = new Option(defaultOptionName, "", true, true);
      select.append(option);
    }
    for (let i = 0; i < data.length; i++) {
      let dataItem = data[i];
      option = new Option(dataItem.name, dataItem.name, false, false);
      select.append(option);
    }
    select.dispatchEvent(new Event("change"));
  }

  function getCountryObjByName(countryName) {
    return allCountries.filter((c) => c.name === countryName);
  }

  function setInitialValue(select) {
    if (select.dataset.value) {
      select.value = select.dataset.value;
      select.dispatchEvent(new Event("change"));
      jQuery(select).trigger("select2:select");
    }
  }

  function changedCountries(e) {
    const countryName = e.target.value;
    if (countryName !== "") {
      const country = getCountryObjByName(countryName);
      citiesSelect.innerHTML = "<option>Loading...</option>";
      loadData(country[0].country_code + "-cities")
        .then(function (cities) {
          generateOptions(cities, citiesSelect, "City");
          setInitialValue(citiesSelect);
        })
        .catch(function (error) {
          generateOptions([{ name: "-" }], citiesSelect);
        });
    } else {
      generateOptions([], citiesSelect, "Select country");
    }
  }

  function listeners() {
    window.addEventListener("load", function () {
      countriesSelect.innerHTML = "<option>Loading...</option>";
      loadData("countries").then(function (countries) {
        allCountries = countries;
        generateOptions(countries, countriesSelect, "Country");
        jQuery(countriesSelect).on("select2:select", changedCountries);
        setInitialValue(countriesSelect);
      });

      generateOptions([], citiesSelect, "Select country");
    });
  }

  listeners();
})();
