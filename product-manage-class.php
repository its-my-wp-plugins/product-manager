<?php

class Prmpr {

    public static $product_fields = array(
        'post_help_type' => array(
			'required' => true,
            'options' => array(18, 19),
            'option_tax_slug' => 'product_cat'
		),
        'post_thumbnail' => array(
            'required' => false,
        ),
		'post_title' => array(
			'required' => true,
            'need_format_string' => true,
		),
        'post_content' => array(
            'required' => true,
        ),
        'post_apartment_country' => array(
            'required' => true,
            'need_format_string' => true,
            // 'required_if' => array(
            //     'name' => 'post_help_type',
            //     'value' => 18
            // )
        ),
        'post_apartment_city' => array(
            'required' => true,
            'need_format_string' => true,
            // 'required_if' => array(
            //     'name' => 'post_help_type',
            //     'value' => 18
            // )
        ),
        'post_apartment_address' => array(
            'required' => true,
            // 'required_if' => array(
            //     'name' => 'post_help_type',
            //     'value' => 18
            // )
        ),
        'post_apartment_places' => array(
            'required' => true,
            'required_if' => array(
                'name' => 'post_help_type',
                'value' => 18
            ),
        ),
        'post_contacts_phone' => array(
            'required' => false,
        ),
        'post_contacts_telegram' => array(
            'required' => false,
        ),
        'post_contacts_viber' => array(
            'required' => false,
        ),
        'post_contacts_whatsapp' => array(
            'required' => false,
        ),
        'post_contacts_gmail' => array(
            'required' => false,
        ),
	);

    public function __construct() {
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue_script'), 99 );
        if( wp_doing_ajax() ){
            add_action( 'wp_ajax_create_product', array($this, 'create_product_wrap') );
            add_action( 'wp_ajax_delete_product', array($this, 'delete_product') );
        }
    }

    public function enqueue_script() {
        wp_enqueue_script( 'prmpr-script', PRMPR_PLUGIN_URL . '/js/ajax.js', array(), '1.0', true );
        wp_localize_script( 'prmpr-script', 'prmprajax', array(
			'url' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('prmprajax-nonce')
		));
    }

    public function create_product_wrap() {

        check_ajax_referer('prmprajax-nonce', 'nonce');

        $validate_res = $this->validate_formdata();

        if(count($validate_res)) {
            echo json_encode(array(
                'success' => false,
                'reqired_fields' => $validate_res,
                'form_status' => 'Fill in the fields correctly'
            ));

            die();
        }

        $data = $this->get_formdata();

        if(isset($_POST['post_id'])) {
            if(hvn_is_current_user_post_author($_POST['post_id'])) {
                $data['ID'] = $_POST['post_id'];
                $res = $this->create_product($data);
            } else {
                echo json_encode(array(
                    'success' => false,
                    'form_status' => 'Sorry, you don\'t have permission to edit this post'
                ));
    
                die();
            }
        } else {
            $res = $this->create_product($data);
        }

        echo json_encode($res);

        die();

	}

    private function get_formdata() {
        $data = array();

        foreach(Prmpr::$product_fields as $key => $field) {
            if(isset($_POST[$key])) {
                $data[$key] = $_POST[$key];

                if(isset($field['need_format_string']) && $field['need_format_string']) {
                    $data[$key] = ucfirst(mb_strtolower($data[$key]));
                }
            }
        }

        return $data;
    }

    private function validate_formdata() {
        $required_fields_error = array();

        foreach(Prmpr::$product_fields as $key => $field) {
            if($field['required'] && (!isset($_POST[$key]) || !strlen($_POST[$key]))) {
                $required_if = $field['required_if'];
                if(isset($required_if) && 
                (!isset($_POST[$required_if['name']]) || ((int) $_POST[$required_if['name']] !== (int) $required_if['value']))) {
                    continue;
                }
                $required_fields_error[] = array(
                    'key' => $key,
                    'message' => 'Field is required'
                );
            }
        }

        return $required_fields_error;

    }

    private function upload_image($field_key, $post_id = 0) {
        
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );
    
        $attachment_id = media_handle_upload( $field_key, $post_id);
    
        if ( is_wp_error( $attachment_id ) ) {
            return array(
                'success' => false,
                'form_status' => 'Image upload error'
            );
        } else {
            return array(
                'success' => true,
                'attachment_id' => $attachment_id
            );
        }
    }

    public function delete_product() {

        check_ajax_referer('prmprajax-nonce', 'nonce');

        if(isset($_POST['post_id'])) {
            if(hvn_is_current_user_post_author($_POST['post_id'])) {
                $product = wc_get_product((int) $_POST['post_id']);
                if($product) {
                    $deleted = $product->delete( $force_delete );
                    
                    echo json_encode(array(
                        'success' => $deleted
                    ));
        
                    die();
                }
            }

            echo json_encode(array(
                'success' => false,
            ));
            die();
        }

        echo json_encode(array(
            'success' => false,
        ));
        die();
    }

    private function create_product($data) {

        $attachment_id = null;

        if(!empty($_FILES)) {
            $attachment_res = $this->upload_image('post_thumbnail');
            if($attachment_res['success']) {
                $attachment_id = $attachment_res['attachment_id'];
            } else {
                return $attachment_res;
            }
        }

        $product_id = wp_insert_post( array(
            'ID' => isset($data['ID']) ? (int) $data['ID'] : '',
            'post_title' => sanitize_text_field( $data['post_title'] ),
            'post_content' => $data['post_content'],
            'post_status' => 'pending',
            'post_type' => "product",
            'post_author'   => get_current_user_id(),
        ) );

        if(isset($_POST['post_thumbnail_deleted']) && $_POST['post_thumbnail_deleted'] === 'true') {
            delete_post_thumbnail($product_id);
        } else {
            set_post_thumbnail($product_id, $attachment_id);
        }

        wp_set_object_terms( $product_id, 'simple', 'product_type' );
        wp_set_object_terms( $product_id, (int) $data['post_help_type'], Prmpr::$product_fields['post_help_type']['option_tax_slug'] );

        $apartment = array(
            'country' => $data['post_apartment_country'],
            'city' => $data['post_apartment_city'],
            'address' => $data['post_apartment_address'],
            'places' => $data['post_apartment_places'],
        );

        update_field( 'field_621b66975dc41', $apartment, $product_id );

        $contacts = array(
            'phone' => $data['post_contacts_phone'],
            'telegram' => $data['post_contacts_telegram'],
            'viber' => $data['post_contacts_viber'],
            'whatsapp' => $data['post_contacts_whatsapp'],
            'gmail' => $data['post_contacts_gmail'],
        );

        update_field( 'field_621b670a5dc47', $contacts, $product_id );

        if( is_wp_error($product_id) ){
            return array(
                'success' => false,
                'form_status' => $product_id->get_error_message()
            );
        } else {
            return array(
                'success' => true,
                'product_id' => $product_id,
                'form_status' => 'Ad has been ' . (isset($data['ID']) ? 'updated' : 'created') . ' and is awaiting moderation.'
            );
        }
    }
}

new Prmpr();