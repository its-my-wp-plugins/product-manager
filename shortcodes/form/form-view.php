<?php
    $post_id = isset($_GET['post_id']) ? $_GET['post_id'] : null;
    if($post_id) {
        $post = get_post($post_id);
        $fileId = get_post_meta( $post_id, '_thumbnail_id', true );
        $filename = basename( get_attached_file( $fileId ) );
        $apartment = get_field('apartment', $post_id);
        $contacts = get_field('contacts', $post_id);
        $post_help_types = get_the_terms( $post->ID, 'product_cat' );
        $post_values = array(
            'post_help_type' => count($post_help_types) ? $post_help_types[0]->term_id : '',
            'post_thumbnail' => $filename,
            'post_title' => $post->post_title,
            'post_content' => $post->post_content,
            'post_apartment_country' => $apartment['country'],
            'post_apartment_city' => $apartment['city'],
            'post_apartment_address' => $apartment['address'],
            'post_apartment_places' => $apartment['places'],
            'post_contacts_phone' => $contacts['phone'],
            'post_contacts_telegram' => $contacts['telegram'],
            'post_contacts_viber' => $contacts['viber'],
            'post_contacts_whatsapp' => $contacts['whatsapp'],
            'post_contacts_gmail' => $contacts['gmail']
        );
    }
?>

<form id="prmpr-form" data-prmpr-form="" method="post" action="#" enctype="multipart/form-data">
    <?php if($post_id): ?>
	    <input type="hidden" name="post_id" id="post_id" value="<?php echo $_GET['post_id']; ?>" />
    <?php endif; ?>
    <div class="prmpr-field-wrap prmpr-field-radio-wrap">
        <div class="prmpr-label" for="post_help_type">Select type of help :</div>
        <div class="prmpr-radios-list">
            <?php $field = Prmpr::$product_fields['post_help_type'] ?>
            <?php foreach($field['options'] as $term_id): ?>
                <?php $term = get_term( $term_id, $field['option_tax_slug'] ); ?>
                <label class="prmpr-radios-list__item radio-ui-label">
                    <input type="radio" name="post_help_type"
                        class="prmpr-field prmpr-required-field radio-input" value="<?= $term_id ?>" <?= $post_id && $term_id === $post_values['post_help_type'] ? 'checked' : '' ?> /><span class="radio-ui"></span><span
                        class="prmpr-radio-caption radio-caption"><?= $term->name ?></span>
                </label>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="prmpr-field-wrap">
        <label class="file-ui-label" for="post_thumbnail" data-file-ui>
            <input type="file" name="post_thumbnail" id="post_thumbnail" class="prmpr-field file-input" multiple="false" value="" hidden />
            <div class="file-ui">
                <div class="btn-file file-ui__btn" role="button" data-file-upload><span class="btn__icon"><?= hvn_get_theme_svg('upload-file-icon') ?></span>Upload</div>
                <div class="file-ui-name file-ui__name" data-file-name-wrap  <?= isset($filename) && !empty($filename) ? '' : 'hidden' ?>><span class="file-ui-name__icon"><?= hvn_get_theme_svg('attached-icon') ?></span><span data-file-name><?= $filename ?></span></div>
                <button class="btn-icon file-ui__remove-btn" data-file-remove <?= isset($filename) && !empty($filename) ? '' : 'hidden' ?>><?= hvn_get_theme_svg('remove-file-icon') ?></button>
            </div>
            <input type="hidden" name="post_thumbnail_deleted" data-file-deleted>
        </label>
    </div>
    <div class="prmpr-field-wrap">
        <label class="prmpr-label" for="post_title">Title :</label>
        <input type="text" name="post_title" id="post_title" class="prmpr-field prmpr-required-field" value="<?= $post_id ? $post_values['post_title'] : '' ?>" />
    </div>
    <div class="prmpr-field-wrap">
        <label class="prmpr-label" for="post_content">Description :</label>
        <textarea name="post_content" id="post_content" class="prmpr-field prmpr-required-field" ><?= $post_id ? $post_values['post_content'] : '' ?></textarea>
    </div>
    <div class="prmpr-group prmpr-group_apartment">
        <div class="prmpr-group__title">Apartment info</div>
        <div class="prmpr-field-wrap">
            <label class="prmpr-label" for="post_apartment_country">Country :</label>
            <select name="post_apartment_country" id="post_apartment_country" class="select2-init prmpr-field prmpr-required-field" data-value="<?= $post_id ? $post_values['post_apartment_country'] : '' ?>" data-select-countries></select>
        </div>
        <div class="prmpr-field-wrap">
            <label class="prmpr-label" for="post_apartment_city">City :</label>
            <select name="post_apartment_city" id="post_apartment_city" class="select2-init prmpr-field prmpr-required-field" data-value="<?= $post_id ? $post_values['post_apartment_city'] : '' ?>" data-select-cities></select>
        </div>
        <div class="prmpr-field-wrap">
            <label class="prmpr-label" for="post_apartment_address">Address :</label>
            <input type="text" name="post_apartment_address" id="post_apartment_address" class="prmpr-field prmpr-required-field" value="<?= $post_id ? $post_values['post_apartment_address'] : '' ?>" />
        </div>
        <div class="prmpr-field-wrap">
            <label class="prmpr-label" for="post_apartment_places">Number of places :</label>
            <input type="number" name="post_apartment_places" id="post_apartment_places" class="prmpr-field" value="<?= $post_id ? $post_values['post_apartment_places'] : '' ?>" />
        </div>
    </div>
    <div class="prmpr-group prmpr-group_contacts">
    <div class="prmpr-group__title">Contact info</div>
    <div class="prmpr-field-wrap">
            <label class="prmpr-label" for="post_contacts_phone"><span class="prmpr-label__icon"><?php echo hvn_get_theme_svg('icon-call') ?></span>Phone :</label>
            <input type="text" name="post_contacts_phone" id="post_contacts_phone" class="prmpr-field" value="<?= $post_id ? $post_values['post_contacts_phone'] : '' ?>" placeholder="Phone number" />
        </div>
        <div class="prmpr-field-wrap">
            <label class="prmpr-label notranslate" for="post_contacts_telegram"><span class="prmpr-label__icon"><?php echo hvn_get_theme_svg('icon-telegram') ?></span>Telegram :</label>
            <input type="text" name="post_contacts_telegram" id="post_contacts_telegram" class="prmpr-field" value="<?= $post_id ? $post_values['post_contacts_telegram'] : '' ?>" placeholder="Username" />
        </div>
        <div class="prmpr-field-wrap">
            <label class="prmpr-label notranslate" for="post_contacts_viber"><span class="prmpr-label__icon"><?php echo hvn_get_theme_svg('icon-viber') ?></span>Viber :</label>
            <input type="text" name="post_contacts_viber" id="post_contacts_viber" class="prmpr-field" value="<?= $post_id ? $post_values['post_contacts_viber'] : '' ?>" placeholder="Phone number" />
        </div>
        <div class="prmpr-field-wrap">
            <label class="prmpr-label notranslate" for="post_contacts_whatsapp"><span class="prmpr-label__icon"><?php echo hvn_get_theme_svg('icon-whatsapp') ?></span>Whatsapp :</label>
            <input type="text" name="post_contacts_whatsapp" id="post_contacts_whatsapp" class="prmpr-field" value="<?= $post_id ? $post_values['post_contacts_whatsapp'] : '' ?>" placeholder="Phone number" />
        </div>
        <div class="prmpr-field-wrap">
            <label class="prmpr-label notranslate" for="post_contacts_gmail"><span class="prmpr-label__icon"><?php echo hvn_get_theme_svg('icon-mail') ?></span>Email :</label>
            <input type="text" name="post_contacts_gmail" id="post_contacts_gmail" class="prmpr-field" value="<?= $post_id ? $post_values['post_contacts_gmail'] : '' ?>" />
        </div>
    </div>
    <div class="prmpr-btn-wrap">
        <?php if($post_id): ?>
            <button type="submit" class="btn" data-prmpr-form-submit="">Update post</button>
        <?php else: ?>
            <button type="submit" class="btn" data-prmpr-form-submit="">Make post</button>
        <?php endif; ?>
    </div>
</form>