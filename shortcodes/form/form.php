<?php
function prmpr_display_form() {
    ob_start();
    require 'form-view.php';
    return ob_get_clean();
}
add_shortcode('prmpr-form', 'prmpr_display_form');