<?php
function prmpr_init_shortcodes() {
    $array = array('form');

    foreach($array as $shortcode_name) {
        include_once PRMPR_PLUGIN_DIR . 'shortcodes/' . $shortcode_name . '/' . $shortcode_name . '.php';
    }
}

prmpr_init_shortcodes();